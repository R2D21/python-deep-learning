# Python deeplearning project

[![forthebadge](https://forthebadge.com/images/badges/made-with-python.svg)](https://forthebadge.com)

I've create this project for who wants learn deep learning and improve my knowledge on this subject. All this exemples are written in python. These exercices where retrived from the coursera platform. 

## For start

More explanation are up to comming

### Prerequisites

More explanation are up to comming

### Installation

More explanation are up to comming

## Quick start

More explanation are up to comming

## Built with

Entrez les programmes/logiciels/ressources que vous avez utilisé pour développer votre projet

* [Python](https://www.python.org/) - Framework CSS (front-end)
* [Atom](https://atom.io/) - Editeur de textes
* [numpy](https://numpy.org/) - Editeur de textes

## Contributing

If you want to contribute. read [CONTRIBUTING.md](https://example.org) file for know how you can contribute

## Autors
* **Rémi Boivin** _alias_ [@R2D21](https://github.com/R2D21)

Read the [contributor's](https://github.com/python-deep-learning/contributors) list if you want to know who has helped

## License

This project and his components are under ``MIT Licence`` - see [LICENSE.md](LICENSE.md) file for more explanations.
